<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    const HIGH_SCORE_LIMIT = 8;

    public function index()
    {
        return User::all();
    }

    public function queryHighScore() {
        return User::orderBy("fastest_time", "asc")
                   ->orderBy("updated_at", "asc")
                   ->orderBy("created_at", "asc")
                   ->limit(static::HIGH_SCORE_LIMIT)
                   ->get();
    }

    public function store(Request $request)
    {
        return User::create([
            "username" => $request->username,
            "fastest_time" => User::INFINITY
        ]);
    }

    public function show($id)
    {
        return User::find($id);
    }

    public function update(Request $request, $id)
    {
        $response = [];

        $user = User::find($id);

        $time = floatval($request->time);
        $response["old_time"] = floatval($user->fastest_time);
        $response["new_time"] = $time;

        $is_faster = $time < $user->fastest_time;

        $response["is_faster"] = $is_faster;

        if($is_faster) {
            $user->fastest_time = $time;
            $user->save();
            $response["message"] = "You're faster than before!";
        } else {
            $response["message"] = "Oops! You're not faster!";
        }

        return $response;
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        unset($user);

        return response()->json(null, 204);
    }
}
