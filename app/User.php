<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public const INFINITY = (0x3f3f3f3f) * 1.0;

    protected $guarded = [];
}