<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get("/", function() {
    return json_encode(["message" => "Hello world!"]);
});

Route::get("/users", "UserController@index");

Route::get("/users/highscore", "UserController@queryHighScore");

Route::post("/users/create", "UserController@store");

Route::get("/users/{id}", "UserController@show");

Route::post("/users/{id}/update", "UserController@update");

Route::get("/users/{id}/delete", "UserController@destroy");